# Yocto project for Rock960

Yocto configuratin for the rock960 platform including various scanner applications
such as [wscanner](https://gitlab.com/gofleetsolutions/wscanner), [fscanner](https://gitlab.com/gofleetsolutions/fscanner) and a brokers such as [mqbroker](https://gitlab.com/gofleetsolutions/mqbroker) and [gpsbroker](https://gitlab.com/gofleetsolutions/gpsbroker).

This project generates the embedded Linux distribution for the rock960 platform.
A specific Docker image is generated to perform the build.

# Usage

Generate the distribution image:
```sh
make build
```

Or run the Docker image to execute Bitbake commands:
```sh
make run
```
