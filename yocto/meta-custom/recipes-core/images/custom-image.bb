SUMMARY = "A small image based on core-image-minimal for RPi target."

LICENSE = "MIT"

IMAGE_FEATURES = "ssh-server-dropbear hwcodecs debug-tweaks"

IMAGE_INSTALL = "\
  ${CORE_IMAGE_EXTRA_INSTALL} \
  kernel-modules \
  packagegroup-core-boot \
  wpa-supplicant \
  iw \
  opencv \
  libopencv-core \
  libopencv-imgproc \
  python3 \
  python3-pip \
  python3-picamera \
  python3-fscanner \
  python3-gpsbroker \
  python3-mqbroker \
  python3-opencv \
  python3-wscanner \
  tcpdump \
  gstreamer1.0-rockchip      \
  gstreamer1.0-meta-base gstreamer1.0-meta-video gstreamer1.0-meta-audio \
  gstreamer1.0-plugins-good-isomp4 gstreamer1.0-plugins-good-multifile \
  gstreamer1.0-plugins-good-imagefreeze \
  gstreamer1.0-plugins-good-video4linux2 \
  gstreamer1.0-plugins-good-rtp \
  gstreamer1.0-plugins-good-rtpmanager \
  gstreamer1.0-plugins-good-udp \
  gstreamer1.0-meta-debug \
  usbutils \
  firmware-rk-wifi \
"
# DISTRO_FEATURES_append = " pam egl gles"

IMAGE_LINGUAS = " "

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"