DESCRIPTION = "ZeroMQ broker"
HOMEPAGE = "https://gitlab.com/gofleetsolutions/mqbroker"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=65c72d7974b747e2762bf57ccb605102"

PV = "0.1+git${SRCPV}"
SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/gofleetsolutions/mqbroker.git;protocol=https;branch=master \
           file://mqbroker.init \
           file://mqbroker.service \
"

S = "${WORKDIR}/git"

inherit setuptools3 systemd update-rc.d

RDEPENDS_${PN}_class-target += " \
    ${PYTHON_PN}-pyzmq \
"

FILES_${PN} += "${sysconfdir}/init.d \
                ${systemd_unitdir}/system/mqbroker.service \
"

SYSTEMD_SERVICE_${PN} = "mqbroker.service"

INITSCRIPT_NAME = "mqbroker"
INITSCRIPT_PARAMS = "defaults 90 10"

do_install_append() {
  install -d ${D}${sysconfdir}/init.d/
  install -m 0755 ${WORKDIR}/mqbroker.init ${D}${sysconfdir}/init.d/mqbroker

  if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/mqbroker.service ${D}${systemd_unitdir}/system/mqbroker.service
  fi
}
