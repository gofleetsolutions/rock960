DESCRIPTION = "Python interface to the Raspberry Pi camera module"
HOMEPAGE = "http://picamera.readthedocs.io/"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=a5f6282f39d52726bdc1e51d5c4b95c9"

SRC_URI[md5sum] = "47e815b0f21bba2a91ab3c4cd36c6f90"
SRC_URI[sha256sum] = "890815aa01e4d855a6a95dd3ad0953b872a6b954982106407df0c5a31a163e50"

export READTHEDOCS="True"

inherit pypi

RDEPENDS_${PN}_class-target += " \
    ${PYTHON_PN}-numpy \
"