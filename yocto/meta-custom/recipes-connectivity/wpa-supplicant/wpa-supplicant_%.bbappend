# Append the wpa-supplicant recipe with custom configuration

do_install_append () {
    install -d ${D}${sysconfdir}

    if [ -e "${WPA_CONFIG}" ]; then
				install -m 600 "${WPA_CONFIG}" "${D}${sysconfdir}/wpa_supplicant.conf"
    fi
}
