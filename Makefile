# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.

CDIR := "$(shell pwd)"
BUILD_DIR := "$(CDIR)/build"
YOCTO_DIR := "$(CDIR)/yocto"

DOCKER_IMAGE := registry.gitlab.com/gofleetsolutions/tools/yocto:latest

all: build-yocto

build-yocto:
		@ if [ ! -d "$(BUILD_DIR)" ]; then \
			cp -rv "$(BUILD_DIR)"-template "$(BUILD_DIR)"; \
		fi

		@ docker run -t \
			--rm \
			--volume="$(YOCTO_DIR)":/home/user/yocto \
			--volume="$(BUILD_DIR)":/home/user/build:rw \
			--name yocto \
			"$(DOCKER_IMAGE)" \
			bash -c 'source /home/user/yocto/poky/oe-init-build-env /home/user/build && bitbake --continue custom-image'

run:
		@ docker run -it \
			--rm \
			--volume="$(YOCTO_DIR)":/home/user/yocto \
			--volume="$(BUILD_DIR)":/home/user/build:rw \
			--name yocto \
			"$(DOCKER_IMAGE)" \
			bash

clean:
		@ rm -rf "$(BUILD_DIR)"
